import Head from "next/head";
import styles from "../styles/Home.module.css";
import GameSelector from "../components/GameSelector";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Star New Game</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <img className={styles.logo} src="assets/PNG/pokemon-title.png" />
      <GameSelector />
    </div>
  );
}
