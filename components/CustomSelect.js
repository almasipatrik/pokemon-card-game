import styles from "../styles/CustomSelect.module.css";

export default function CustomSelect({ options, name, onSelect, label }) {
  return (
    <>
      <select
        className={styles.select}
        onChange={onSelect}
        name={name}
        id={name}
      >
        <option value="" disabled selected>
          {label}
        </option>
        {options.map((option) => {
          return <option value={option.toLowerCase()}>{option}</option>;
        })}
      </select>
    </>
  );
}
