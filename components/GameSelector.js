import CustomButton from "../components/CustomButton";
import CustomSelect from "../components/CustomSelect";
import localStorageKeys from "../enums/storage-keys.enum";
import styles from "../styles/GameSelector.module.css";
import { useState } from "react";
import { useRouter } from "next/router";

export default function GameSelector() {
  const initValue = "2";
  const router = useRouter();
  const [deckSize, setDeckSize] = useState(initValue);

  const handleStartGame = () => {
    localStorage.setItem(localStorageKeys.DECK_SIZE, deckSize);
    router.push("in-game");
  };

  return (
    <div className={styles.container}>
      <div className={styles.wrapper}>
        <CustomSelect
          label="Deck size"
          onSelect={(e) => setDeckSize(e.target.value)}
          options={[initValue, "4", "6", "8"]}
        />
        <CustomButton onClick={() => handleStartGame()}>
          Start new game
        </CustomButton>
      </div>
    </div>
  );
}
