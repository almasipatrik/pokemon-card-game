import styles from "../../styles/InGame.module.css";
import Head from "next/head";
import CustomHeader from "../../components/CustomHeader";
import Card from "../../components/Card";
import localStorageKeys from "../../enums/storage-keys.enum";
import { useState, useEffect } from "react";
import CustomButton from "../../components/CustomButton";
import { v4 as uuidv4 } from "uuid";
import Router from "next/router";

export default function InGame() {
  const [deck, setDeck] = useState([]);

  const [firstGuess, setFirstGuess] = useState();
  const [secondGuess, setSecondGuess] = useState();
  const [score, setScore] = useState(0);

  const [currentTries, setCurrentTries] = useState(0);

  useEffect(() => {
    checkIfMatch();
  }, [secondGuess]);

  const checkIfMatch = () => {
    incrementTries();

    if (isMatch()) setScore(score + 1);

    setFirstGuess(null);
    setSecondGuess(null);
    refreshCards();
  };

  const refreshCards = () => {
    if (deck.length > 0) {
      const tempDeck = deck;
      tempDeck.forEach((card) => {
        if (card.guessed === true) card.hide = false;
        else card.hide = true;
      });

      setDeck(tempDeck);
    }
  };

  const isMatch = () => {
    if (
      firstGuess &&
      secondGuess &&
      firstGuess.id === secondGuess.id &&
      firstGuess.index !== secondGuess.index
    )
      return true;

    return false;
  };

  const initGame = () => {
    const createdDeck = createDeck();
    shuffleDeck(createdDeck);
  };

  const incrementTries = () => {
    if (firstGuess && secondGuess) setCurrentTries(currentTries + 1);
  };

  const selectCard = (selectedCard) => {
    const card = deck[selectedCard.index];

    if (!firstGuess && card.index !== selectedCard.index && card != firstGuess)
      setFirstGuess(card);
    else if (
      !secondGuess &&
      card.index !== selectedCard.index &&
      card !== firstGuess
    )
      setSecondGuess(card);
  };

  const createDeck = () => {
    const deckSize = localStorage.getItem(localStorageKeys.DECK_SIZE);

    const currentDeck = [];
    for (let i = 0; i < deckSize; i++) {
      let id = uuidv4();
      currentDeck.push({
        id,
        index: i,
        hide: true,
        pokeImageSrc: `card-poke${i + 1}`,
        guessed: false,
      });
      currentDeck.push({
        id: id,
        index: i + 1,
        hide: true,
        pokeImageSrc: `card-poke${i + 1}`,
        guessed: false,
      });
    }
    return currentDeck;
  };

  const shuffleDeck = (deck) => {
    const tempDeck = Object.assign([], deck);
    const shuffledDeck = [];

    while (tempDeck.length > 0) {
      const randomNumber = Math.floor(Math.random() * tempDeck.length);
      const randomCard = tempDeck[randomNumber];
      shuffledDeck.push(randomCard);

      tempDeck.splice(randomNumber, 1);
    }

    setDeck(shuffledDeck);
  };

  useEffect(() => {
    initGame();
  }, []);

  return (
    <div className={styles.container}>
      <Head>
        <title>Playing</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <CustomHeader />
      <div className={styles.info}>
        <h1 className={styles.h1}>Current tries: {currentTries}</h1>
        <h1 className={styles.h1}>Best: {score}</h1>
        <CustomButton onClick={() => Router.reload(window.location.pathname)}>
          Restart
        </CustomButton>
      </div>
      <div className={styles.cards}>
        {deck.length
          ? deck.map((card, index) => (
              <Card
                guessed={card.guessed}
                isHidden={card.hide}
                key={index}
                uniqueKey={index}
                id={card.id}
                onSelect={selectCard}
                pokeImageSrc={card.pokeImageSrc}
              />
            ))
          : "Loading ..."}
      </div>
    </div>
  );
}
