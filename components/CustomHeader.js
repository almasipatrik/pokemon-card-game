import CustomButton from "./CustomButton";
import { useRouter } from "next/router";
import localStorageKeys from "../enums/storage-keys.enum";
import GameSelector from "./GameSelector";
import styles from "../styles/CustomHeader.module.css";

export default function CustomHeader() {
  const router = useRouter();

  const navigateBackToHome = () => {
    localStorage.clear(localStorageKeys.DECK_SIZE);
    router.push("/");
  };

  return (
    <nav className={styles.nav}>
      <CustomButton className={styles.home} onClick={navigateBackToHome}>
        Home
      </CustomButton>
      <GameSelector />
    </nav>
  );
}
