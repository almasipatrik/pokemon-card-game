import styles from "../styles/Card.module.css";
import { useState } from "react";

export default function Card({
  pokeImageSrc,
  onSelect,
  id,
  guessed,
  uniqueKey,
  hide,
}) {
  const [key, setKey] = useState(uniqueKey);
  const [uniqueId, setUniqueId] = useState(id);
  const [isHidden, setHidden] = useState(!hide);
  const [isGuessed, setIsGuessed] = useState(guessed);

  const handleShow = () => {
    if (!isGuessed) setHidden(hide);
  };

  return (
    <div
      className={styles.card}
      onClick={() => {
        onSelect({ id, index: key });
        handleShow();
      }}
    >
      <img
        src={
          isHidden
            ? "/assets/PNG/card-back.png"
            : `/assets/PNG/${pokeImageSrc}.png`
        }
      />
    </div>
  );
}
